import { makeStyles } from "@material-ui/core/styles";
const useStyles = makeStyles((theme) => ({
  textFight: { textAlign: "center" },
  rootFop: {
    flexGrow: 1,
  },
  rootFight: {
    textAlign: "center",
    maxWidth: 355,
    minWidth: 350,
  },
  mediaFight: {
    height: 240,
  },
  root: {
    padding: "2px 4px",
    margin: "0 auto 20px auto",

    display: "flex",
    alignItems: "center",
    width: "50%",
  },
  paperRoot: {
    display: "flex",
    flexWrap: "wrap",
    "& > *": {
      margin: theme.spacing(1),
      width: theme.spacing(16),
      height: theme.spacing(16),
    },
  },
  iconButtonClose: {
    position: "absolute !important",
    top: 0,
    right: 0,
  },
  paperst: {
    padding: 20,
    position: "relative",
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  iconclear: {
    marginRight: theme.spacing(3),
  },
  icon: {
    marginRight: theme.spacing(2),
  },
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6),
  },
  heroButtons: {
    marginTop: theme.spacing(4),
  },
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  card: {
    height: "100%",
    display: "flex",
    flexDirection: "column",
    cursor: "pointer",
  },
  cardMedia: {
    paddingTop: "56.25%", // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(6),
  },
  pos: {
    marginBottom: 12,
  },
  title: {
    fontSize: 14,
  },
  extendedIcon: {
    marginRight: theme.spacing(1),
  },
  cardChip: {
    marginRight: 3,
    boxShadow:
      "0px 3px 3px -2px rgba(0,0,0,0.2), 0px 3px 4px 0px rgba(0,0,0,0.14), 0px 1px 8px 0px rgba(0,0,0,0.12)",
    marginTop: 3,
  },
  mauto: {
    margin: "0 auto",
  },
  selected: {
    height: "100%",
    display: "flex",
    flexDirection: "column",
    cursor: "pointer",
    border: "solid 2px #3f51b5",
  },
  full: { display: "block", width: "100%" },
}));

export default useStyles;
