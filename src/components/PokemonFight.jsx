import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import CardMedia from "@material-ui/core/CardMedia";
import Life from "./Life";
export default function PokemonFight(props) {
  if (!props.fight || !props.fight.selected) return null;
  const pk = props.opponent ? props.fight.opponent : props.fight.selected;
  //normalize life value
  const normMalize = (value, MAX) => (value * 100) / MAX;

  // disable attacks while loading opponent or not the turn

  const dsbld = !props.opponent && (props.fight.fightLoading || !props.myTurn);
  console.log(
    props.opponent +
      "op | load:" +
      props.fight.fightLoading +
      "   myt: " +
      props.myTurn +
      "         res: " +
      dsbld
  );
  if (pk.image) {
    return (
      <Card className={props.classes.rootFight}>
        <CardActionArea>
          <CardMedia
            className={props.classes.mediaFight}
            image={pk.image}
            title={pk.name}
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
              {pk.name}
            </Typography>
            <Life
              value={Math.round(normMalize(props.life, pk.maxCP))}
              color={props.opponent ? "secondary" : "primary"}
            />
          </CardContent>
        </CardActionArea>

        {!props.opponent &&
          pk.attacks &&
          pk.attacks.fast &&
          pk.attacks.fast.length > 0 && (
            <CardActions>
              {" "}
              <ButtonGroup
                size="small"
                color="primary"
                aria-label="large outlined primary button group"
              >
                {pk.attacks.fast.map((at, i) => {
                  const damage = at.damage;
                  const type = at.type;
                  const name = at.name;
                  return (
                    <Button
                      key={"at" + i}
                      disabled={dsbld}
                      onClick={() => {
                        props.doAttack(damage, type, name);
                      }}
                    >
                      {at.name}
                    </Button>
                  );
                })}
              </ButtonGroup>{" "}
            </CardActions>
          )}
        {!props.opponent &&
          pk.attacks &&
          pk.attacks.special &&
          pk.attacks.special.length > 0 && (
            <CardActions>
              {" "}
              <ButtonGroup
                size="small"
                color="secondary"
                aria-label="large outlined primary button group"
              >
                {pk.attacks.special.map((at, i) => {
                  const damages = at.damage;
                  const types = at.type;
                  const names = at.name;
                  return (
                    <Button
                      key={"ats" + i}
                      disabled={dsbld}
                      onClick={() => {
                        props.doAttack(damages, types, names);
                      }}
                    >
                      {at.name}
                    </Button>
                  );
                })}
              </ButtonGroup>{" "}
            </CardActions>
          )}
      </Card>
    );
  } else return null;
}
