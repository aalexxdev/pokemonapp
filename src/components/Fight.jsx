import React from "react";
import Paper from "@material-ui/core/Paper";
import IconButton from "@material-ui/core/IconButton";
import ClearIcon from "@material-ui/icons/Clear";
import Grid from "@material-ui/core/Grid";
import PokemonFight from "./PokemonFight";
import loading from "../assets/loadingOpponent.gif";
import fightImage from "../assets/fight.fw.png";
import EndGame from "./EndGame";
function Fight(props) {
  const [myLife, getAttack] = React.useState(props.fight.selected.maxCP);
  const [opLife, setAttack] = React.useState(0);
  const [text, setText] = React.useState("   ");
  const [turn, setTurn] = React.useState(props.fight.turn);
  const [gameEnd, setGameEnd] = React.useState({
    show: false,
    title: "",
    text: "",
  });
  React.useEffect(() => {
    const filtered = props.pokemons.filter((p) => {
      return p.id !== props.fight.selected.id;
    });
    const randomOpponent = Math.floor(Math.random() * filtered.length);
    props.loadOpponent(true);
    setAttack(props.pokemons[randomOpponent].maxCP);
    props.setOpponent(props.pokemons[randomOpponent]);
  }, []);

  // msg with action is set to user, then reseted and turn changes.
  const changeText = (op) => {
    setTimeout(() => {
      setText("  ");
      !op ? attack() : setTurn(op);
    }, 4000);
  };

  // OPPONENT ATTACK
  const attack = () => {
    const options = props.fight.opponent.attacks.fast.concat(
      props.fight.opponent.attacks.special
    );
    const randomAttack = Math.floor(Math.random() * options.length);
    const attack = options[randomAttack];
    let dmgAt = attack.damage + 50;
    let textOp =
      "<<<<<< " + props.fight.opponent.name + " attacks with " + attack.name;
    if (props.fight.selected.weaknesses.indexOf(attack.type) >= 0) {
      textOp += ", and it is your debility!";
      dmgAt = dmgAt * 2;
    } else if (props.fight.selected.resistant.indexOf(attack.type) >= 0) {
      dmgAt = dmgAt / 2;
      textOp += ", and you are resistant!";
    }
    if (myLife - dmgAt <= 0) {
      setGameEnd({
        show: true,
        title: "YOU LOOSE!",
        text: " Sorry, but you loose against " + props.fight.opponent.name,
      });
      setText("  ");
    } else {
      getAttack(myLife - dmgAt);
      textOp += " Total damage: " + dmgAt;
      setText(textOp);

      changeText(true);
    }
  };
  // YOUR ATTACK
  const doAttack = (damage, type, name) => {
    // setTurn(false);
    let texto = ">>>>>>  You attack with " + name;
    // dmg * 2 to finish before the fight
    let dmg = damage + 50;
    if (props.fight.opponent.weaknesses.indexOf(type) >= 0) {
      texto += ", and it is a debility!";
      dmg = dmg * 2;
    } else if (props.fight.opponent.resistant.indexOf(type) >= 0) {
      dmg = dmg / 2;
      texto += ", and it is resistant!";
    }
    texto += " Total damage: " + dmg;
    if (opLife - dmg <= 0) {
      setGameEnd({
        show: true,
        title: "YOU WIN!",
        text: " You won the battle against " + props.fight.opponent.name,
      });
      setAttack(0);
      setText("  ");
    } else {
      setAttack(opLife - dmg);
      setText(texto);
      changeText(false);
    }
  };

  const endGame = () => {
    setGameEnd({ show: false, title: "", text: "" });
    props.doFight({
      enabled: false,
      selected: {},
      opponent: {},
      fightLoading: false,
      turn: true,
    });
  };
  return (
    <Paper className={props.classes.paperst}>
      <EndGame
        show={gameEnd.show}
        title={gameEnd.title}
        text={gameEnd.text}
        reset={() => {
          endGame();
        }}
      />
      <img src={fightImage} height="50px" alt="Fight Logo" />
      <IconButton
        type="reset"
        onClick={() => {
          endGame();
        }}
        className={props.classes.iconButtonClose}
        aria-label="search"
      >
        <ClearIcon />
      </IconButton>
      <Grid container className={props.classes.rootFop} spacing={2}>
        <Grid item className={props.classes.textFight} sm={12} md={12} lg={12}>
          {text}
        </Grid>

        <Grid item xs={12} sm={6} md={6} lg={6}>
          <PokemonFight
            myTurn={turn}
            fight={props.fight}
            doAttack={(damage, type, name) => {
              doAttack(damage, type, name);
              setTurn(false);
            }}
            life={myLife}
            opponent={false}
            classes={props.classes}
          />
        </Grid>
        <Grid item>
          {props.fight.fightLoading && (
            <img src={loading} alt="Loading Oponnent" />
          )}
          {!props.fight.fightLoading && (
            <PokemonFight
              opponent={true}
              life={opLife}
              fight={props.fight}
              classes={props.classes}
            />
          )}
        </Grid>
      </Grid>
    </Paper>
  );
}

export default Fight;
