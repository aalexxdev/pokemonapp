import Chip from "@material-ui/core/Chip";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";

function Pokemon(props) {
  const clickFightHandler = (e) => {
    e.preventDefault();
    const fData = Object.assign(props.fightData, {
      enabled: true,
      selected: props.pokemon,
    });
    props.doFight(fData);
  };
  const elev =
    props.selectedPokemon && props.selectedPokemon.id === props.pokemon.id
      ? 0
      : 3;
  const classSelected =
    props.onlyName &&
    props.selectedPokemon &&
    props.selectedPokemon.id === props.pokemon.id
      ? props.classes.selected
      : props.classes.card;
  return (
    <Card
      onClick={() => {
        props.selPokemon(props.pokemon, props.selectedPokemon);
      }}
      elevation={elev}
      className={classSelected}
    >
      <CardMedia
        className={props.classes.cardMedia}
        image={props.pokemon.image}
        title={props.pokemon.name}
      />
      <CardContent className={props.classes.cardContent}>
        {!props.onlyName && (
          <Typography
            className={props.classes.title}
            color="textSecondary"
            gutterBottom
          >
            {props.pokemon.classification}
          </Typography>
        )}
        <Typography gutterBottom variant="h5" component="h2">
          {props.pokemon.name}
        </Typography>
        {!props.onlyName && (
          <Typography className={props.classes.pos} color="textSecondary">
            {"MaxCP " + props.pokemon.maxCP + " MaxHP: " + props.pokemon.maxHP}
          </Typography>
        )}
        {!props.onlyName && (
          <div>
            {props.pokemon.resistant.map((item, i) => {
              return (
                <Chip
                  key={"res" + i}
                  variant="outlined"
                  elevation={2}
                  className={props.classes.cardChip}
                  label={item}
                  color="primary"
                  size="small"
                />
              );
            })}
            <br />
            {props.pokemon.weaknesses.map((item, i) => {
              return (
                <Chip
                  key={"weak" + i}
                  variant="outlined"
                  className={props.classes.cardChip}
                  label={item}
                  color="secondary"
                  size="small"
                />
              );
            })}
          </div>
        )}
      </CardContent>
      {!props.onlyName && (
        <CardActions>
          <Button
            onClick={(e) => clickFightHandler(e)}
            className={props.classes.full}
            variant="contained"
            color="secondary"
          >
            Select and Fight!
          </Button>
        </CardActions>
      )}
    </Card>
  );
}

export default Pokemon;
