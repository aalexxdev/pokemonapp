import Toolbar from "@material-ui/core/Toolbar";
import AppBar from "@material-ui/core/AppBar";
import logo from "../assets/logo192.png";
import SportsKabaddiIcon from "@material-ui/icons/SportsKabaddi";
function Header(classes) {
  return (
    <AppBar position="static">
      <Toolbar>
        <img src={logo} alt="Logo" />
      </Toolbar>
    </AppBar>
  );
}
export default Header;
