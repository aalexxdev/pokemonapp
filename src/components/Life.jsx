import LinearProgress from "@material-ui/core/LinearProgress";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";

function Life(props) {
  const MAX = 100;
  const normalise = (value, MAX) => (value * 100) / MAX;
  return (
    <Box display="flex" alignItems="center">
      <Box width="100%" mr={1}>
        <LinearProgress
          color={props.color || "primary"}
          variant="determinate"
          value={props.value}
        />
      </Box>
      <Box minWidth={35}>
        <Typography variant="body2" color="textSecondary">
          {props.value}
        </Typography>
      </Box>
    </Box>
  );
}

export default Life;
