import SlidingPanel from "react-sliding-side-panel";
import "react-sliding-side-panel/lib/index.css";

import mobile from "is-mobile";
import Pokemon from "./Pokemon";
import Button from "@material-ui/core/Button";
const LeftPanel = (props) => {
  return (
    <SlidingPanel
      style={{ backgroundColor: "white" }}
      type={"right"}
      isOpen={props.selected !== null}
      size={mobile() ? 60 : 30}
      backdropClicked={() => props.selPokemon(null)}
    >
      <div onClick={() => props.selPokemon(null)}>
        <Button
          onClick={() => props.selPokemon(null)}
          className={props.classes.full}
          variant="contained"
        >
          Close
        </Button>

        {props.selected !== null && (
          <Pokemon
            fightData={props.fightData}
            doFight={props.doFight}
            onlyName={false}
            classes={props.classes}
            selPokemon={() => props.selPokemon(null)}
            selectedPokemon={props.selected}
            pokemon={props.selected}
          />
        )}
      </div>
    </SlidingPanel>
  );
};

export default LeftPanel;
