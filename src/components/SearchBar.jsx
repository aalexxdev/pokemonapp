import Paper from "@material-ui/core/Paper";
import InputBase from "@material-ui/core/InputBase";
import IconButton from "@material-ui/core/IconButton";
import ClearIcon from "@material-ui/icons/Clear";
import SearchIcon from "@material-ui/icons/Search";

export default function SearchBar(props) {
  const submit = (e) => {
    e.preventDefault();
    props.setSearch(props.search);
  };
  return (
    <Paper component="form" className={props.classes.root}>
      <InputBase
        onChange={(e) => props.setSearch(e.currentTarget.value)}
        value={props.search}
        className={props.classes.input}
        placeholder="Pokemon name"
        inputProps={{ "aria-label": "Search Pokemon" }}
      />

      <IconButton
        type="reset"
        onClick={() => {
          props.setSearch("");
        }}
        className={props.classes.iconButton}
        aria-label="search"
      >
        <ClearIcon />
      </IconButton>

      <IconButton
        type="submit"
        onClick={(e) => submit(e)}
        className={props.classes.iconButton}
        aria-label="search"
      >
        <SearchIcon />
      </IconButton>
    </Paper>
  );
}
