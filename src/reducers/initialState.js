const initialState = {
  pokemons: [],
  error: false,
  loading: true,
  user: null,
  fight: {
    enabled: false,
    selected: {},
    opponent: {},
    fightLoading: false,
    turn: true,
  },
  search: "",
  selected: null,
};
export default initialState;
