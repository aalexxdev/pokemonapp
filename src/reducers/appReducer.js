import initialState from "./initialState";
import {
  IS_ERROR,
  IS_LOADING,
  FETCH_DATA,
  RECEIVE_DATA,
  SET_SEARCH,
  POKEMON_SELECTED,
  SET_FIGHT,
  SET_FIGHT_LOADING,
  SET_OPPONENT,
} from "../actions/allActions";

export default function appRed(state = initialState, action) {
  switch (action.type) {
    case "persist/REHYDRATE":
      return initialState;
    case SET_SEARCH:
      return {
        ...state,
        search: action.search,
      };
    case SET_FIGHT:
      return {
        ...state,
        fight: action.fight,
      };
    case FETCH_DATA:
      return {
        ...state,
        loading: true,
        register: null,
      };
    case RECEIVE_DATA:
      return {
        ...state,
        pokemons: action.data,
        loading: false,
      };
    case IS_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
      };
    case IS_LOADING:
      return {
        ...state,
        loading: action.loading,
      };
    case POKEMON_SELECTED:
      return {
        ...state,
        selected: action.selected,
      };
    case SET_FIGHT_LOADING:
      return {
        ...state,
        fight: {
          ...state.fight,
          fightLoading: action.fightLoading,
        },
      };
    case SET_OPPONENT:
      return {
        ...state,
        fight: {
          ...state.fight,
          opponent: action.opponent,
        },
      };
    default:
      return state;
  }
}
