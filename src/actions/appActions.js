import {
  IS_ERROR,
  IS_LOADING,
  RECEIVE_DATA,
  POKEMON_SELECTED,
  SET_SEARCH,
  SET_FIGHT,
  SET_FIGHT_LOADING,
  SET_OPPONENT,
} from "./allActions";
import { ApolloClient, gql, InMemoryCache } from "@apollo/client";

const client = new ApolloClient({
  uri: "https://graphql-pokemon2.vercel.app/",
  cache: new InMemoryCache(),
});

export function receiveData(data) {
  return {
    type: RECEIVE_DATA,
    data,
  };
}

export function fight(fight) {
  return {
    type: SET_FIGHT,
    fight,
  };
}
export function setOpponent(opponent) {
  return {
    type: SET_OPPONENT,
    opponent,
  };
}
export function setError(error) {
  return {
    type: IS_ERROR,
    error,
  };
}

export const isError = (error) => {
  return (dispatch) => {
    dispatch(setError(error));
    setTimeout(() => {
      dispatch(setError(null));
    }, 3000);
  };
};

export function isLoading(loading) {
  return {
    type: IS_LOADING,
    loading,
  };
}

export function setLoadingFight(fightLoading) {
  return {
    type: SET_FIGHT_LOADING,
    fightLoading,
  };
}

export const setFightLoading = (val) => {
  return (dispatch) => {
    dispatch(setLoadingFight(val));
    setTimeout(() => {
      dispatch(setLoadingFight(false));
    }, 5000);
  };
};

export const fetchData = (search = "") => {
  return (dispatch) => {
    dispatch(isLoading(true));
    client
      .query({
        query: gql`
          {
            pokemons(first: 151) {
              id
              number
              name
              weight {
                minimum
                maximum
              }
              height {
                minimum
                maximum
              }
              attacks {
                fast {
                  name
                  type
                  damage
                }
                special {
                  name
                  type
                  damage
                }
              }
              classification
              types
              resistant
              weaknesses
              fleeRate
              maxCP
              maxHP
              image
            }
          }
        `,
      })
      .then(function (res) {
        dispatch(receiveData(res.data.pokemons));
        dispatch(isLoading(false));
      })
      .catch(function (error) {
        let err =
          error.response && error.response.data && error.response.data.message
            ? error.response.data.message
            : "ERROR OCURRED (FETCH POKEMONS)";
        dispatch(isError(err));
      });
  };
};

export function selPokemon(selected) {
  return { type: POKEMON_SELECTED, selected };
}

export function setSearch(search) {
  return { type: SET_SEARCH, search };
}

export const search = (search) => {
  return (dispatch) => {
    dispatch(setSearch(search));
  };
};
