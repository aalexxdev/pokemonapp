import { connect } from "react-redux";
import React from "react";
import Footer from "./components/Footer";
import { bindActionCreators } from "redux";
import * as appActions from "./actions/appActions";
import Header from "./components/Header";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import useStyles from "./styles/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Pokemon from "./components/Pokemon";
import SearchBar from "./components/SearchBar";
import LeftPanel from "./components/LeftPanel";
import Typography from "@material-ui/core/Typography";
import Fight from "./components/Fight";
function App(props) {
  const classes = useStyles();
  /*fetch initial api*/

  React.useEffect(() => {
    props.appActions.fetchData();
  }, []);
  const selPok = (newPokemon, current) => {
    if (current === null) {
      props.appActions.selPokemon(newPokemon);
    } else if (newPokemon.id === current.id) {
      props.appActions.selPokemon(null);
    } else {
      props.appActions.selPokemon(newPokemon);
    }
  };

  const results = () => {
    const pattern = props.search
      .split("")
      .map((x) => {
        return `(?=.*${x})`;
      })
      .join("");
    const regex = new RegExp(`${pattern}`, "ig");

    const res = props.pokemons.filter((pkm) => {
      return pkm.name.match(regex);
    });
    return res;
  };

  const pk = props.search !== "" ? results() : props.pokemons;

  return (
    <div>
      <CssBaseline />
      <LeftPanel
        classes={classes}
        selPokemon={(s) => props.appActions.selPokemon(s)}
        selected={props.selected}
        fightData={props.fight}
        doFight={(fight) => props.appActions.fight(fight)}
      />
      <Header {...classes} />
      <main>
        <Container className={classes.cardGrid} maxWidth="md">
          {(props.fight.enabled && (
            <Fight
              setOpponent={(op) => {
                props.appActions.setOpponent(op);
              }}
              pokemons={props.pokemons}
              classes={classes}
              loadOpponent={(v) => {
                props.appActions.setFightLoading(v);
              }}
              endFight={(d) => {
                props.appActions.endFight(d);
              }}
              fight={props.fight}
              doFight={(fight) => {
                props.appActions.fight(fight);
              }}
            />
          )) || (
            <div>
              <SearchBar
                setSearch={(s) => {
                  props.appActions.search(s);
                }}
                search={props.search}
                classes={classes}
              />
              <Grid container spacing={4}>
                {props.search !== "" && pk.length <= 0 && (
                  <Typography
                    gutterBottom
                    variant="h5"
                    clasName={classes.mauto}
                    component="h2"
                  >
                    NO RESULTS
                  </Typography>
                )}
                {pk &&
                  pk.length > 0 &&
                  pk.map((pokemon, i) => (
                    <Grid item key={i} xs={12} sm={6} md={3}>
                      <Pokemon
                        onlyName={true}
                        selPokemon={selPok}
                        selectedPokemon={props.selected}
                        classes={classes}
                        pokemon={pokemon}
                      />
                    </Grid>
                  ))}
              </Grid>
            </div>
          )}
        </Container>
      </main>

      <Footer classes={classes} loading={props.loading} />
    </div>
  );
}

function mapStateToProps(state) {
  return {
    pokemons: state.appRed.pokemons,
    search: state.appRed.search,
    fight: state.appRed.fight,
    error: state.appRed.error,
    loading: state.appRed.loading,
    modal: state.appRed.modal,
    selected: state.appRed.selected,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    appActions: bindActionCreators(appActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
